# API

The API has two methods:
1. generate
2. retrieve

## Application Runtime

Use the docker-compose.


```bash
docker-compose up
```
OR
```bash
sudo docker-compose up
```

## After The Container Is Started

Run from the container.

```bash
composer install
```

## An Example Of Endpoints
1. http://localhost:8500/api/generate
2. http://localhost:8500/api/retrieve?id=1

## Authorization JWT
By adding the "Authorization" header, eg "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2Mzg3NTgxMDMsImlhdCI6MTYzODc1Nzk4M30.f2Z_CjQwaoR3UXv-RwaojQOkW-RvaojQUXvao".

## phpMyAdmin
url: http://localhost:8181/

user: root

password: test
