<?php

use App\Bootstrap;
use App\Response;
use App\HttpStatusCode;
use App\Lib\AuthService;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

require './vendor/autoload.php';
require './config/routes.php';
require './config/db.php';
require './config/jwt.php';

try {
    $headers = getallheaders();

    if (empty($headers['Authorization'])) {
        throw new \Exception(HttpStatusCode::UNAUTHORIZED['name'], HttpStatusCode::UNAUTHORIZED['code']);
    }

    $isValidAuthJwt = AuthService::verificationJwt($jwt['key'], $headers['Authorization']);

    if (!$isValidAuthJwt) {
        throw new \Exception(HttpStatusCode::FORBIDDEN['name'], HttpStatusCode::FORBIDDEN['code']);
    }

    $paths = ['./src/Entity'];
    $isDevMode = true;
    $proxyDir = null;
    $cache = null;
    $useSimpleAnnotationReader = false;

    $config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode, $proxyDir, $cache, $useSimpleAnnotationReader);
    $entityManager = EntityManager::create($dbParams, $config);

    $bootstrap = new Bootstrap($routes, $headers, $entityManager);

    $bootstrap->run();
} catch (Exception $exc) {
    $message = $exc->getMessage();
    $code = $exc->getCode();

    if (!in_array($code, HttpStatusCode::HttpStatusesSupported)) {
        $code = HttpStatusCode::INTERNAL_SERVER_ERROR['code'];
    }

    Response::end(
        [
            'statusCode' => $code,
            'message' => $message
        ],
        $code
    );
}
