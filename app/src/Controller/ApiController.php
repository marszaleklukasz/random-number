<?php

namespace App\Controller;

use App\HttpStatusCode;
use App\Response;
use App\Lib\RandomNumbers;
use App\Entity\RandomNumber;

class ApiController
{
    private $data = null;
    private $entityManager = null;

    public function __construct($data, $entityManager)
    {
        $this->data = $data;
        $this->entityManager = $entityManager;
    }

    public function generate()
    {
        $randomNumberValue = RandomNumbers::generate();

        $randomNumber = new RandomNumber();
        $randomNumber->setRandomNumber($randomNumberValue);

        $this->entityManager->persist($randomNumber);
        $this->entityManager->flush();

        Response::end(
            [
                'statusCode' => HttpStatusCode::OK['code'],
                'data' => [
                    'id' => $randomNumber->getId(),
                    'random_number' => $randomNumberValue
                ]
            ],
            HttpStatusCode::OK['code']
        );
    }

    public function retrieve()
    {
        $parameters = null;
        $id = null;

        $repository = $this->entityManager->getRepository(RandomNumber::class);

        if (!empty($this->data['parameters']['id'])) {
            $id = $this->data['parameters']['id'];
        }

        if (!empty($id)) {
            $randomNumber = $repository->find($id);

            if (!empty($randomNumber)) {
                Response::end(
                    [
                        'statusCode' => HttpStatusCode::OK['code'],
                        'data' => [
                            'id' => $randomNumber->getId(),
                            'random_number' => $randomNumber->getRandomNumber()
                        ]
                    ],
                    HttpStatusCode::OK['code']
                );
            } else {
                throw new \Exception('RandomNumber ' . HttpStatusCode::NOT_FOUND['name'], HttpStatusCode::NOT_FOUND['code']);
            }
        } else {
            throw new \Exception('RandomNumber ' . HttpStatusCode::BAD_REQUEST['name'], HttpStatusCode::BAD_REQUEST['code']);
        }
    }
}
