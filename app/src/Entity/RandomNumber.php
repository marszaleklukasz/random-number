<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RandomNumber
 *
 * @ORM\Table(name="random_number")
 * @ORM\Entity
 */
class RandomNumber
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="random_number", type="bigint", nullable=false)
     */
    private $randomNumber;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set randomNumber.
     *
     * @param int $randomNumber
     *
     * @return RandomNumber
     */
    public function setRandomNumber($randomNumber)
    {
        $this->randomNumber = $randomNumber;

        return $this;
    }

    /**
     * Get randomNumber.
     *
     * @return int
     */
    public function getRandomNumber()
    {
        return $this->randomNumber;
    }
}
