<?php

namespace App\Lib;

use Firebase\JWT\JWT;

class AuthService
{
    public static function verificationJwt(string $configValue, string $userValue)
    {
        return self::verificationJwtHash($configValue, $userValue);
    }

    private static function verificationJwtHash(string $configValue, string $userValue) {
        $result = false;

        try {
            $authorizationHash = self::getJwtToken($userValue);

            $decoded = JWT::decode($authorizationHash, $configValue, ['HS256']);

            if (!empty($decoded)) {
                $result = true;
            }
        } catch (\Exception $e) {

        }

        return $result;
    }

    private static function getJwtToken(string $userValue) {
        return str_replace('Bearer ', '', $userValue);
    }
}
