<?php

namespace App;

use App\Router;
use App\HttpStatusCode;

class Bootstrap
{
    private $route = null;
    private $parseUrl = null;
    private $headers = null;
    private $entityManager = null;

    public function __construct($routes, $headers, $entityManager)
    {
        $parameters = null;

        $this->headers = $headers;
        $this->entityManager = $entityManager;

        $this->parseUrl = parse_url(urldecode($_SERVER['REQUEST_URI']));

        if (!empty($this->parseUrl['query'])) {
            parse_str($this->parseUrl['query'], $parameters);

            if (!empty($parameters)) {
                $this->parseUrl['parameters'] = $parameters;
            }
        }

        $router = new Router($routes);

        $this->route = $router->getRoute($this->parseUrl['path']);
    }

    public function run(): void
    {
        if (empty($this->route)) {
            throw new \Exception(HttpStatusCode::NOT_FOUND['name'], HttpStatusCode::NOT_FOUND['code']);
        }

        $className = '\\App\\Controller\\' . $this->route['controller'];
        $actionName = $this->route['action'];

        (new $className($this->parseUrl, $this->entityManager))->$actionName();
    }
}
